package students

import (
	"github.com/gin-gonic/gin"
	"go/pkg/repository"
)

func StudentRoutes(r *gin.Engine) {
	router := r.Group("api/v1/student/") //group of request endpoint
	router.GET("findById/:docId", repository.FindById)
	router.GET("findByRevID/:rev", repository.FindByRevId)
	router.GET("insert", repository.AddStudent)
	router.DELETE("delete-doc/:docId/:revId", repository.DeleteDocById)
	router.PUT("update-doc/:docId", repository.UpdateDocById)
	router.GET("filter-by-class/:classname", repository.FilterByClass)

}
