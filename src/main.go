package main

import (
	"github.com/gin-gonic/gin"
	_ "github.com/go-kivik/couchdb/v3" // The CouchDB driver
	"go/common/config"
	"go/pkg/students"
)

func main() {
	config.Handelcouchdb()
	router := gin.New()            //initualized gin package
	students.StudentRoutes(router) //register rout
	router.Run(":8080")            //run app on port 8080
}
